@extends('app')
@section('content')
<div>
    <form action="/confirm" method="post" style="margin-top: 30vh">
        @csrf

        <div class="form-outline mb-4">
            <input type="number" name="code" id="form2Example1" class="form-control" />
            <label class="form-label" for="form2Example1">Confirm code</label>
        </div>

        <!-- Submit button -->
        <button type="submit" class="btn btn-primary btn-block mb-4">Sign in</button>

    </form>
</div>
@endsection