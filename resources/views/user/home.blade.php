@extends('app')

@section('content')
<a href="/logout">Log Out</a>

<div class="center">

    <table id="customers" style="margin-top: 30vh">
        <tr>
            <th>User Name</th>
            <th>Email</th>
            <th>Role</th>
        </tr>
        @foreach($users as $user)
        <tr>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            <td>{{$user->role->name}}</td>
        </tr>
        @endforeach
    </table>

</div>
@endsection