@extends('app')
@section('content')


<div class="center" style="margin-top: 30vh">

    <form action="/user/{{$user->id}}" method="post">
        @csrf
        @method('PUT')
        <div class="form-outline mb-4">
            <input type="text" name="name" value="{{$user->name}}" id="form2Example1" class="form-control" />
            <label class="form-label" for="form2Example1">Name</label>
        </div>

        <div class="form-outline mb-4">
            <input type="email" name="email" value="{{$user->email}}" id="form2Example1" class="form-control" />
            <label class="form-label" for="form2Example1">Email address</label>
        </div>

        <!-- Password input -->
        <div class="form-outline mb-4">
            <input type="password" name="password" id="form2Example2" class="form-control" />
            <label class="form-label" for="form2Example2">Password</label>
        </div>


        <!-- Submit button -->
        <button type="submit" class="btn btn-success btn-block mb-4">Create</button>
    </form>

</div>


@endsection