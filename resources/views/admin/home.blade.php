@extends('app')
@section('content')

<a href="/logout">Log Out</a>

<div class="center" style="margin-top: 30vh">

	<button type="button" class="btn btn-success  mb-3" style="float: right"> <a href="/user/create"> Add User </a></button>
	<table id="customers">
		<tr>
			<th>User Name</th>
			<th>Email</th>
			<th>Role</th>
			<th>Settings</th>
		</tr>
		@foreach($users as $user)
		<tr>
			<td>{{$user->name}}</td>
			<td>{{$user->email}}</td>
			<td>{{$user->role->name}}</td>
			<td class="d-flex">
				<form action="/user/{{$user->id}}" method="get">
					<button type="submit" style="    background: none;    border: none;">
						<!-- <i class="fa fa-car"></i> -->
						Edit
						<i class="fa-solid fa-pen"></i>
					</button>
				</form>
				<form action="/user/{{$user->id}}" method="POST">
					@csrf
					@method('DELETE')
					<button type="submit" style="    background: none;    border: none;">
						<!-- <i class="fa fa-car"></i> -->
						Delete
						<i class="fa-solid fa-trash"></i>
					</button>
				</form>
			</td>
		</tr>
		@endforeach
	</table>

</div>


@endsection