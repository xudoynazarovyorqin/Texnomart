<?php

use App\Http\Controllers\LoginController;
use App\Http\Controllers\TwoStepVertificationController;
use App\Http\Controllers\UserController;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (Auth::user()) return redirect('home');

    return  view('auth.login');
})->name('login');

Route::post('/vertify', [LoginController::class, 'sendVerification']);
Route::middleware('auth')->group(function () {

    Route::post('/confirm', [LoginController::class, 'confirmVerification']);
    Route::middleware('verified')->group(function () {

        Route::resource('/user', UserController::class);
        Route::get('/home', [UserController::class, 'index'])->name('user-home');
    });
     
    Route::get('/logout', function () {
    
        auth()->logout();
        
        return redirect('/');
    });
});
