<?php

namespace App\Http\Middleware;

use App\Models\User;
use App\Models\UserActivity;
use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EmailVerify
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {

        $user = Auth::user();

        if (
            $user &&
            $user->email_verify_at !== null &&
            !$user->email_verify_at->lt(
                Carbon::now()
                    && $this->activityCheck($user)
            )
        ) {
            return $next($request);
        }

        $user->resetTwoFactorCode();
        auth()->logout();

        return redirect()->route('login');
    }

    private function activityCheck($user)
    {
        $activity =  UserActivity::firstOrNew(['user_id' => $user->id]);

        if ($activity->activity_count == 1 && Carbon::parse($activity->email_verify_date)->lt(now()->subMinutes(10))) {

            auth()->logout();
            $activity->delete();
            User::find($user->id)->resetTwoFactorCode();

            return false;
        }

        $activity->activity_count = $activity->activity_count + 1;
        $activity->save();

        // return true;
    }
}
