<?php

namespace App\Http\Middleware;

use App\Models\User;
use App\Models\UserActivity;
use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $user = Auth::user();

        if (!$user ) {
            return redirect()->route('login');
        }

        return $next($request);
    }

    // private function activityCheck($user)
    // {
    //     $activity =  UserActivity::firstOrNew(['user_id' => $user->id]);

    //     if ($activity->activity_count == 0 && Carbon::parse($activity->email_verify_date)->lt(now()->addMinutes(10))) {

    //         auth()->logout();
    //         // $activity->delete();
    //         User::find($user->id)->resetTwoFactorCode();

    //         return true;
    //     }

    //     $activity->activity_count = $activity->activity_count + 1;
    //     $activity->save();

    //     return false;
    // }
}
