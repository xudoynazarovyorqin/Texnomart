<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            //   testlash uchun kommentga olindi
            //  'email' => 'required|unique:users|exists:users|max:100',
             'email' => 'required|exists:users|max:100',
             'password' => 'required|min:6|max:100'
        ];
    }

    public function messages()
    {
        return [
            'email.exists' => "This email hasn't in database.",
            'email.required' => "Email must be required",
            'password.required' => 'Password must be required.',
            'password.min' => 'Password must min 6 character.',
        ];
    }
}
