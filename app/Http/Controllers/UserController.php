<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Repository\User\UserInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    protected $user;

    public function __construct(UserInterface $user)
    {
        $this->user = $user;
    }

    public function index()
    {
        if (!Auth::user()) {
            return redirect()->route('login');
        }
        
        $users = $this->user->getAllData();

        if (Auth::user()->role->slug == 'user') {

            return view('user.home')->with('users', $users);
        }
        if (Auth::user()->role->slug == 'admin') {

            return view('admin.home')->with('users', $users);
        }
    }

    public function create()
    {
        return view('admin.add-user');
    }

    public function store(Request $request)
    {
        $this->user->store($request);

        return redirect()->route('user-home');
    }

    public function update($id, Request $request)
    {
        $this->user->update($id, $request);

        return redirect()->route('user-home');
    }

    public function show($id)
    {
        dd(request());
        $user = $this->user->view($id);

        return view('admin.edit-user')->with('user', $user);
    }

    public function destroy($id)
    {
        $this->user->delete($id);

        return redirect()->route('user-home');
    }
}
