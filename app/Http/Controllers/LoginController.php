<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Mail\VerificationEmail;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class LoginController extends Controller
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }


    public function sendVerification(LoginRequest $request)
    {
        $user = $this->user->where('email', $request->email)->first();

        $user->authAttempt($request, $user->id);

        if ('admin' == $user->role->slug) {

            $user->setEmailVertifyDate(5);

            return redirect()->route('user-home');
        }

        if ($user->two_factor_expires_at != null && !$user->two_factor_expires_at->lt(now())) {

            return redirect()->route('user-home');
        }
        // dd($request->email);
        Mail::to($request->email)->send(new VerificationEmail($request->validated()));

        return view('auth.confirm-email');
    }

    public function confirmVerification(Request $request)
    {
        $user = $this->user->find(Auth::user()->id);

        $code = $request->code;

        if ($user->two_factor_expires_at->lt(now())) {

            $user->resetTwoFactorCode();

            auth()->logout();

            return redirect()->route('login');
        }

        if ($code == $user->two_factor_code) {

            $user->setEmailVertifyDate(5);

            return redirect()->route('user-home');
        }
    }
}
