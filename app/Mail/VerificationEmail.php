<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VerificationEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $email;
    protected $password;


    public function __construct($request)
    {
        $this->email = $request['email'];
        $this->password = bcrypt($request['password']);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $rand_code = rand(100000, 999999);
        
        User::where('email', $this->email)->update([
            'two_factor_code'           => $rand_code,
            'two_factor_expires_at'     => now()->addMinutes(10)
        ]);


        return $this->markdown('mail.verification-email')->with('rand_code', $rand_code);
    }
}
