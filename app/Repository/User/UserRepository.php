<?php

namespace App\Repository\User;

use App\Models\User;
use function is;
use function is_null;

class UserRepository implements UserInterface
{
    public function getAllData()
    {
        return User::latest()->get();
    }

    public function update($id = null, $data)
    {
        if (!$user = User::find($id)) {
            return false;
        }

        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = bcrypt($data['password']);
        $user->role_id = 2;

        return $user->save();
    }

    public function store($data)
    {
        $user = new User();
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = bcrypt($data['password']);
        $user->role_id = 2;

        return $user->save();
    }

    public function view($id)
    {
        return User::find($id);
    }
    public function delete($id)
    {
        return User::find($id)->delete();
    }
}
