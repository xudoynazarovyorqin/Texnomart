<?php

namespace App\Repository\User;

use App\Models\User;

interface UserInterface{
    public function getAllData();
    public function store($data);
    public function update($id = null,$data);
    public function view($id);
    public function delete($id);
}