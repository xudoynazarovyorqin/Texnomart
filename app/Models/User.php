<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */

    protected $dates = [
        'id',
        'updated_at',
        'created_at',
        'deleted_at',
        'email_verify_at',
        'two_factor_expires_at',
    ];


    protected $fillable = [
        'name',
        'email',
        'password',
        'created_at',
        'updated_at',
        'role_id',
        'remember_token',
        'email_verify_at',
        'two_factor_code',
        'two_factor_expires_at',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function resetTwoFactorCode()
    {
        $this->two_factor_code = null;
        $this->email_verify_at = null;
        $this->two_factor_expires_at = null;
        $this->save();
    }

    public function authAttempt($request, $user_id)
    {
        UserActivity::updateOrCreate(['user_id' => $user_id]);

        Auth::attempt([
            'email' => $request->email,
            'password' => $request->password
        ]);
    }

    public function setEmailVertifyDate($day)
    {
        $this->email_verify_at = Carbon::now()->addDays($day);
        $this->save();
        
        UserActivity::updateOrCreate(['user_id' => $this->id], ['email_verify_date' => now(), 'activity_count' => 0]);
    }

    public function activityCheck($user)
    {
        $activity =  UserActivity::firstOrNew(['user_id' => $user->id]);

        if ($activity->activity_count == 0 && $activity->email_verify_date->lt(now()->addMinutes(10))) {

            auth()->logout();
            $this->resetTwoFactorCode();

            return true;
        }

        $activity->activity_count = $activity->activity_count + 1;
        $activity->save();

        return false;
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }
}
