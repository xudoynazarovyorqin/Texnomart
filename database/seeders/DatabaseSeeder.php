<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $role = \App\Models\Role::firstOrCreate([
            'name'        => 'Admin',
            'slug'           => "admin",
        ]);
        \App\Models\Role::firstOrCreate([
            'name'        => 'User',
            'slug'           => "user",
        ]);
        \App\Models\User::firstOrCreate([
            'name'        => 'admin',
            'email'       => "texnomart@gmail.com",
            'password'    => bcrypt('adminadmin'),
            'role_id'     => $role->id
        ]);
    }
}
